package junitcucumber;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junitcucumber.User;

public class UserSteps {
	
	private User user = new User();

	@Given("^that the user (.*) is given a task to clear (.*) certification exam$")
	public void certificationName(String name, String certication) throws Throwable {
		user.setName(name);
		user.setCertification(certication);
	}

	@When("^(.*) got (\\d+) marks in exam$")
	public void gotMarks(String name, int marks) throws Throwable {
		user.setName(name);
		user.setMarks(marks);
	}

	@Then("^(.*) (.*) (.*) certified$")
	public void certifiedYes(String name,String certStatus, String certification) throws Throwable {
		assertThat(name, is(user.getName()));
		assertThat(user.getCertification(), equalTo("Java"));
		if(certStatus.equals("is")) {
			assertThat(user.getResult(), is(true));
		}else if(certStatus.equals("isn't")){
			assertThat(user.getResult(), is(false));
		}else{
			assertThat(true, is(false));
		}	
	}
	
	@Given("^exam cost (\\d+)$")
	public void exam_cost(int price) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Given("^(.*) pay (\\d+) for the Java certification$")
	public void pay_for_the_Java_certification(String username, int price) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^(.*) (.*) give the exam$")
	public void can_give_the_exam(String name,String certStatus) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}
}
