Feature: User Certification
	Check that users with 60 mark in exam pass the certification exam
	and users with less than 60 fail the exam

	Background:
		Given that the user "oscar" is given a task to clear Java certification exam

  Scenario: User is Passed
    When "oscar" got 60 marks in exam
    Then "oscar" is Java certified
    
  Scenario: User not Passed
    When "oscar" got 50 marks in exam
    Then "oscar" isn't Java certified
