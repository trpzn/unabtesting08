Feature: User Payment
	Check that users can't take a certification that isn't payed

	Background:
		Given that the user "oscar" is given a task to clear Java certification exam
		And exam cost 400

  Scenario: User try to take certification paying the right amount
  	And "oscar" pay "500" for the Java certification
    Then "oscar" can give the exam
    
  Scenario: User try to take certification paying less
  	And "oscar" pay "200" for the Java certification
    Then "oscar" can't give the exam
